# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://gitlab.com/guardianproject-ops/ansible-nginx/compare/0.1.0...0.1.1) (2020-07-02)


### Features

* Bump nginx role dep to 0.14.0 ([c7822f8](https://gitlab.com/guardianproject-ops/ansible-nginx/commit/c7822f8869752c020dd37362b2449f9da59b01da))

## [0.1.0] - 2020-04-14

### Added

- This CHANGELOG
- Initial version

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-nginx/compare/0.1.0...master
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-nginx/-/tags/0.1.0
