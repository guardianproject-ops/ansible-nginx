## Role Variables

* `nginx_enable`: `true` - Install NGINX



* `nginx_start`: `true` - Start NGINX service



* `nginx_log_rotate_days`: `10` - Logs files are rotated daily. After `nginx_log_rotate_days` they are deleted.



* `nginx_log_rotate_maxsize`: `100000000` - Log files are rotated when they grow bigger than size bytes even before the daily interval



* `nginx_branch`: `mainline` - Specify which branch of NGINX Open Source you want to install. Options are 'mainline' or 'stable'.



* `nginx_version`: `''` - Specify which version of NGINX you want to install.



* `nginx_log_format_with_ip`: `default nginx log format with ip` - The log format used with name 'with_ip' that contains ips



* `nginx_log_format_noip`: `nginx log format without ips` - The log format used with name 'main' that doesn't contain ips



* `nginx_log_format`: `{{ nginx_log_format_noip }}` - The default log format



* `nginx_error_log_level`: `alert` - The nginx error log level, set to alert to increase privacy



* `nginx_remove_default_conf`: `true` - Set to true to remove the default nginx vhost


